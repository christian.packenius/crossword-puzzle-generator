package com.packenius.crosswordpuzzlegenerator;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.*;

/**
 * Erstellt aus vorgegebenen Key-Value-Paaren eine Art Kreuzworträtsel, bei dem die Schlüssel
 * die Fragen sind und die Werte die Antworten.
 */
public class CrosswordPuzzleGenerator {
  static final Object WORD_END = new Object();

  public static void main(String[] args) throws Exception {
    workInputFile("docs/Unit 3 - Teil 04 - Seite 217.vokabeln");
  }

  /**
   * Erstellt ein Rätsel aus den Zeilenpaaren (jeweils eine Zeile Frage und eine Zeile Antwort) der Datei.
   */
  public static void workInputFile(String filename) throws IOException {
    System.out.println(filename);

    // Datei als Zeilenliste einlesen.
    List<String> lines = Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);

    int timesToTry = 2000;
    createAndOpenPuzzle(lines, timesToTry);
  }

  /**
   * Erstellt und öffnet das Kreuzworträtsel.
   */
  public static void createAndOpenPuzzle(List<String> lines, int timesToTry) throws IOException {
    // Leer- und Kommentarzeilen löschen.
    for (int i = lines.size() - 1; i >= 0; i--) {
      String line = lines.get(i).trim();
      if (line.isEmpty() || line.startsWith("#")) {
        lines.remove(i);
      }
    }

    // Es muss eine gerade Anzahl an Vokabeln vorliegen!
    if (lines.size() % 2 != 0) {
      throw new RuntimeException("Die Anzahl an Zeilen ist nicht gerade.");
    }

    // Maximal 99 Vokabeln!
    if (lines.size() > 198) {
      throw new RuntimeException("Zu viele Rätsel, bitte maximal 99 Paare verwenden.");
    }

    long puzzleSize = Long.MAX_VALUE;
    String _puzzle = "";
    String _questionsText = "";

    // Mehrfach versuchen, ein möglichst kompaktes Puzzle zu erstellen.
    for (int i = 0; i < timesToTry; i++) {
      // Alle bzw. die restlichen noch einzutragenden Vokabeln.
      List<Quest> quests = new ArrayList<>();

      // In dieser Map wird zu jeder Koordinate das zu zeichnende Objekt enthalten sein.
      // Dies ist das eigentliche Raster, in dem gezeichnet wird.
      Map<Coordinate, Object> coor2obj = new HashMap<>();

      // In dieser Map wird zu jedem Buchstaben (Zeichen) eine Liste von Koordinaten enthalten sein.
      // Dies dient nur dem einfacheren Auffinden bereits gesetzter Zeichen.
      Map<Character, List<Coordinate>> char2coors = new HashMap<>();

      // Aus der Liste eine Map machen.
      for (int m = 0; m < lines.size(); m += 2) {
        String question = lines.get(m);
        String answer = lines.get(m + 1);
        quests.add(new Quest(question, answer));
      }

      // Und durcheinander wirbeln, durchnummerieren und nochmal durcheinander bringen.
      Collections.shuffle(quests);
      String questionsText = "";
      for (int k = 0; k < quests.size(); k++) {
        quests.get(k).setID(k + 1);
        questionsText += (k + 1) + ":&nbsp;" + quests.get(k).question.replace(" ", "&nbsp;") + "&nbsp;--- ";
      }


      // Erste Vokabel am Nullpunkt platzieren.
      PlacedQuest placedQuest = new PlacedQuest(quests.remove(0), new Coordinate(0, 0), Direction.RIGHT);
      ScoredPlace scoredPlace = new ScoredPlace(placedQuest, 0);
      insertQuest(scoredPlace, coor2obj, char2coors);

      // Alle weiteren Vokabeln nacheinander eintragen.
      while (!quests.isEmpty()) {
        ScoredPlace best = findBestPlaceForNextAnswer(quests, coor2obj, char2coors);
        quests.remove(best.placedQuest.quest);
        insertQuest(best, coor2obj, char2coors);
      }

      String puzzle = createPuzzle(coor2obj);
      if (getPuzzleMaxSize(coor2obj) < puzzleSize) {
        _puzzle = puzzle;
        _questionsText = questionsText;
        puzzleSize = getPuzzleMaxSize(coor2obj);
        // System.out.println("-> Aktuelle Berechnungsgröße: " + puzzleSize);
      }
    }

    // Datei wegschreiben.
    createPuzzleFile(_puzzle, _questionsText);

    // Kreuzworträtsel im Browser öffnen.
    Desktop.getDesktop().browse(Paths.get("puzzle.html").toUri());
  }

  private static ScoredPlace findBestPlaceForNextAnswer(List<Quest> quests, Map<Coordinate, Object> coor2obj, Map<Character, List<Coordinate>> char2coors) {
    // Für jede Vokabel prüfen, wo die beste Stelle ist und wie gut sie dort passt.
    ScoredPlace best = null;
    for (Quest quest : quests) {
      ScoredPlace curr = findBestPlaceToCombine(quest, coor2obj, char2coors);
      if (best == null || curr != null && curr.score > best.score) {
        best = curr;
      }
    }

    // Wenn wir hier nichts gefunden haben, muss ein Wort irgendwo gesetzt
    // werden, wo es nicht mit einem anderen verbunden ist.
    if (best == null) {
      for (Quest quest : quests) {
        ScoredPlace curr = findMinimalBestPlace(quest, coor2obj);
        if (best == null || curr != null && curr.score > best.score) {
          best = curr;
        }
      }
    }
    return best;
  }

  private static void createPuzzleFile(String _puzzle, String _questionsText) throws FileNotFoundException {
    try (PrintStream out = new PrintStream(new FileOutputStream("puzzle.html"), false, StandardCharsets.UTF_8)) {
      out.println("<!DOCTYPE html>\n<html>\n<head>\n<title>Kreuzworträtsel</title>\n<meta charset=\"UTF-8\">\n</head>\n<body>\n<pre>");
      out.println(_puzzle);
      out.println("\n" + "</pre>\n" + "\n");
      out.println();
      out.println(_questionsText);
      out.println("\n</body>\n</html>\n");
    }
  }

  /**
   * Ermittelt die Größe des Kreuzworträtsels, die der längeren Kante entspricht.
   */
  private static int getPuzzleMaxSize(Map<Coordinate, Object> coor2obj) {
    int xmin = coor2obj.keySet().stream().mapToInt(key -> key.x).min().getAsInt();
    int ymin = coor2obj.keySet().stream().mapToInt(key -> key.y).min().getAsInt();
    int xmax = coor2obj.keySet().stream().mapToInt(key -> key.x).max().getAsInt();
    int ymax = coor2obj.keySet().stream().mapToInt(key -> key.y).max().getAsInt();

    return Math.max(ymax - ymin, xmax - xmin);
  }

  /**
   * Schreibt das Wort an die angegebene Koordinate im "Raster", das durch die beiden
   * Maps dargestellt wird.
   */
  private static void insertQuest(ScoredPlace scoredPlace, Map<Coordinate, Object> coor2obj, Map<Character, List<Coordinate>> char2coors) {
    PlacedQuest placedQuest = scoredPlace.placedQuest;
    Direction direction = scoredPlace.placedQuest.direction;
    Quest quest = placedQuest.quest;
    quest.direction = direction;
    Coordinate coor = placedQuest.coordinate;
    String answer = quest.answer;

    // An nullter Stelle wird die Frage angegeben.
    if (coor2obj.get(coor) != null) {
      throw new RuntimeException("Programmfehler #01.");
    }
    coor2obj.put(coor, quest);

    // Dann die einzelnen Antwortzeichen ins Raster einfügen.
    for (int i = 0; i < answer.length(); i++) {
      coor = coor.change(direction);
      Object content = coor2obj.get(coor);
      Character character = answer.charAt(i);

      // Raster füllen.
      if (content == null) {
        coor2obj.put(coor, character);
      } else if (!content.equals(character)) {
        throw new RuntimeException("Programmfehler #02.");
      }

      // In Zeichenlisten eintragen.
      List<Coordinate> coorList = char2coors.computeIfAbsent(character, k -> new ArrayList<>());
      coorList.add(coor);
    }

    coor = coor.change(direction);
    coor2obj.put(coor, WORD_END);
  }

  /**
   * Sucht einen Platz für das Wort, wo es möglichst viele Feldern anderer Worte überlappt.
   */
  private static ScoredPlace findBestPlaceToCombine(Quest quest, Map<Coordinate, Object> coor2obj, Map<Character, List<Coordinate>> char2coors) {
    ScoredPlace wordBest = null;
    String answer = quest.answer;

    // Für jeden Buchstaben der Antwort den besten Platz finden.
    for (int i = 0; i < answer.length(); i++) {
      Character character = answer.charAt(i);
      List<Coordinate> coorList = char2coors.get(character);
      if (coorList != null) {
        // Alle Koordinaten ausprobieren, an denen dieser Buchstabe bereits steht.
        for (Coordinate coor : coorList) {
          Direction direction1 = Direction.RIGHT;
          Direction direction2 = Direction.DOWN;
          if (Math.random() < 0.5) {
            direction1 = Direction.DOWN;
            direction2 = Direction.RIGHT;
          }
          ScoredPlace scoredPlace = getScore(quest, coor, i, direction1, coor2obj);
          ScoredPlace scoredPlace2 = getScore(quest, coor, i, direction2, coor2obj);
          if (scoredPlace == null) {
            scoredPlace = scoredPlace2;
          } else if (scoredPlace2 != null) {
            if (scoredPlace2.score > scoredPlace.score) {
              scoredPlace = scoredPlace2;
            }
          }
          if (scoredPlace != null && (wordBest == null || scoredPlace.score > wordBest.score)) {
            wordBest = scoredPlace;
          }
        }
      }
    }

    return wordBest;
  }

  /**
   * Prüft, ob die Antwort an den angegebenen Koordinaten Platz hat. Falls nicht, wird
   * <i>null</i> zurück gegeben. Falls doch, enthält der "Score" die Anzahl an Zeichen,
   * die mit anderen bereits platzierten Wörtern überein stimmen.
   */
  private static ScoredPlace getScore(Quest quest, Coordinate coor, int delta, Direction direction,
                                      Map<Coordinate, Object> coor2obj) {
    coor = coor.change(direction, -(delta + 1));
    Coordinate start = coor;
    String answer = quest.answer;

    // Hier muss Platz für den Quest sein.
    if (coor2obj.get(coor) != null) {
      return null;
    }

    // Alle Buchstabenfelder müssen leer oder der richtige Buchstabe sein.
    int score = 0;
    for (int i = 0; i < answer.length(); i++) {
      coor = coor.change(direction);
      Object obj = coor2obj.get(coor);
      if (obj == null) {
        continue;
      }
      if (!(obj instanceof Character)) {
        return null;
      }
      if (!obj.equals(answer.charAt(i))) {
        return null;
      }
      score++;
    }

    // Hinter dem letzten Buchstaben muss eine freie Stelle oder ein Ende-Zeichen sein.
    coor = coor.change(direction);
    Object obj = coor2obj.get(coor);
    if (obj != null && obj != WORD_END) {
      return null;
    }

    return new ScoredPlace(new PlacedQuest(quest, start, direction), score);
  }

  /**
   * Eine Stelle suchen, wo halt keine Verbindung zu anderen Worten besteht.
   */
  private static ScoredPlace findMinimalBestPlace(Quest quest, Map<Coordinate, Object> coor2obj) {
    // Abgrenzungen feststellen.
    int xmin = coor2obj.keySet().stream().mapToInt(key -> key.x).min().getAsInt();
    int ymin = coor2obj.keySet().stream().mapToInt(key -> key.y).min().getAsInt();
    int xmax = coor2obj.keySet().stream().mapToInt(key -> key.x).max().getAsInt();
    int ymax = coor2obj.keySet().stream().mapToInt(key -> key.y).max().getAsInt();

    // Wort nach rechts geschrieben - freien Platz suchen.
    xmax -= quest.answer.length() + 1;
    for (int y = ymin; y <= ymax; y++) {
      for (int x = xmin; x <= xmax; x++) {
        ScoredPlace scoredPlace = getScore(quest, new Coordinate(x, y), 0, Direction.RIGHT, coor2obj);
        if (scoredPlace != null) {
          return scoredPlace;
        }
      }
    }

    // Wort nach unten geschrieben - freien Platz suchen.
    xmax += quest.answer.length() + 1;
    ymax -= quest.answer.length() + 1;
    for (int y = ymin; y <= ymax; y++) {
      for (int x = xmin; x <= xmax; x++) {
        ScoredPlace scoredPlace = getScore(quest, new Coordinate(x, y), 0, Direction.DOWN, coor2obj);
        if (scoredPlace != null) {
          return scoredPlace;
        }
      }
    }

    // Einfach an die erst-beste Stelle packen, von oben nach unten probieren. Letzter Ausweg.
    for (int i = 0; ; i++) {
      ScoredPlace scoredPlace = getScore(quest, new Coordinate(0, i), 0, Direction.RIGHT, coor2obj);
      if (scoredPlace != null) {
        return scoredPlace;
      }
    }
  }

  /**
   * Erzeugt das Puzzle in einem einzigen String.
   */
  private static String createPuzzle(Map<Coordinate, Object> coor2obj) {
    // Alle Wort-Ende-Zeichen löschen.
    List<Coordinate> keys = new ArrayList<>(coor2obj.keySet());
    for (int i = keys.size() - 1; i >= 0; i--) {
      if (coor2obj.get(keys.get(i)) == WORD_END) {
        coor2obj.remove(keys.get(i));
      }
    }

    // Abgrenzungen feststellen.
    int xmin = coor2obj.keySet().stream().mapToInt(key -> key.x).min().getAsInt();
    int ymin = coor2obj.keySet().stream().mapToInt(key -> key.y).min().getAsInt();
    int xmax = coor2obj.keySet().stream().mapToInt(key -> key.x).max().getAsInt();
    int ymax = coor2obj.keySet().stream().mapToInt(key -> key.y).max().getAsInt();

    // Die erste Variante ist zu unübersichtlich, daher eine zweite.
    return createPuzzleVariante2(coor2obj, xmin, ymin, xmax, ymax);
  }

  /**
   * Erstellt ein Puzzle, das aber über die vielen ungültigen
   * Felder ### sehr unübersichtlich wird.
   */
  @SuppressWarnings("unused")
  private static String createPuzzleVariante1(Map<Coordinate, Object> coor2obj, int xmin, int ymin, int xmax, int ymax) {
    StringBuilder field = new StringBuilder();

    // Für jede Spalte in der obersten Reihe eine Linie.
    field.append("┌");
    for (int x = xmin; x <= xmax; x++) {
      field.append("───");
      if (x < xmax) {
        field.append("┬");
      } else {
        field.append("┐");
      }
    }
    field.append("\r\n");

    for (int y = ymin; y <= ymax; y++) {
      // Erster Durchlauf: Nur von den Quest-Objekten die Nummer angeben, wenn RIGHT.
      field.append("│");
      for (int x = xmin; x <= xmax; x++) {
        Object obj = coor2obj.get(new Coordinate(x, y));
        if (obj == null) {
          field.append("###│");
        } else if (obj instanceof Character) {
          if (obj.equals(' ')) {
            field.append(" · │");
          } else {
            field.append("   │"); // Das soll erraten werden.
            // field.append(" " + obj + " │"); // Hier die Lösung dazu.
          }
        } else {
          Quest quest = (Quest) obj;
          String pfeil = quest.direction.arrow;
          field.append(quest.id + pfeil + "│");
        }
      }
      field.append("\r\n");

      // Abschlusslinie hinter Zeile.
      if (y < ymax) {
        field.append("├");
        for (int x = xmin; x <= xmax; x++) {
          if (x < xmax) {
            field.append("───┼");
          } else {
            field.append("───┤");
          }
        }
        field.append("\r\n");
      }
      // Allerletzte Textzeile?
      else {
        field.append("└");
        for (int x = xmin; x <= xmax; x++) {
          if (x < xmax) {
            field.append("───┴");
          } else {
            field.append("───┘");
          }
        }
        field.append("\r\n");
      }
    }

    return field.toString();
  }

  /**
   * Erstellt ein Puzzle, bei dem die überflüssigen Felder nicht mehr mit
   * eingezeichnet werden.<br>
   * .1.<br>
   * 8+2<br>
   * .4.<br>
   * Dies ist das Muster für die Kreuze.
   */
  // ----------------------0123456789abcdef
  static char[] crosses = " ╵╶└╷│┌├╴┘─┴┐┤┬┼".toCharArray();

  private static String createPuzzleVariante2(Map<Coordinate, Object> coor2obj, int xmin, int ymin, int xmax, int ymax) {
    StringBuilder field = new StringBuilder();

    for (int y = ymin; y <= ymax + 1; y++) {
      // Linie oberhalb der y-Zeile ziehen.
      for (int x = xmin; x <= xmax + 1; x++) {
        int cross = 0;
        cross |= coor2obj.containsKey(new Coordinate(x - 1, y - 1)) ? 9 : 0;
        cross |= coor2obj.containsKey(new Coordinate(x, y - 1)) ? 3 : 0;
        cross |= coor2obj.containsKey(new Coordinate(x - 1, y)) ? 12 : 0;
        cross |= coor2obj.containsKey(new Coordinate(x, y)) ? 6 : 0;
        field.append(crosses[cross]);

        if (x <= xmax) {
          if (coor2obj.containsKey(new Coordinate(x, y)) || coor2obj.containsKey(new Coordinate(x, y - 1))) {
            field.append("───");
          } else {
            field.append("   ");
          }
        }
      }
      field.append("\r\n");

      // Der eigentliche Inhalt der Zeile.
      for (int x = xmin; x <= xmax + 1; x++) {
        // Trennzeichen zwischen zwei Zellen.
        if (coor2obj.containsKey(new Coordinate(x - 1, y)) || coor2obj.containsKey(new Coordinate(x, y))) {
          field.append("│");
        } else {
          field.append(" ");
        }

        // Inhalt.

        // Leer? Falls rundherum noch gefüllte Zellen sind, dies mit "###" kennzeichnen,
        // damit es nicht fälschlicherweise als zu füllende Zelle interpretiert wird.
        if (!coor2obj.containsKey(new Coordinate(x, y))) {
          if (coor2obj.containsKey(new Coordinate(x - 1, y))
            && coor2obj.containsKey(new Coordinate(x, y - 1))
            && coor2obj.containsKey(new Coordinate(x + 1, y))
            && coor2obj.containsKey(new Coordinate(x, y + 1))) {
            field.append("###");
          } else {
            field.append("   ");
          }
        }

        // Fragestellung?
        else if (coor2obj.get(new Coordinate(x, y)) instanceof Quest) {
          Quest quest = (Quest) coor2obj.get(new Coordinate(x, y));
          String pfeil = quest.direction.arrow;
          field.append(quest.id + pfeil);
        }

        // Zu füllendes Zeichen?
        else if (coor2obj.get(new Coordinate(x, y)) instanceof Character) {
          Character ch = (Character) coor2obj.get(new Coordinate(x, y));
          if (ch == ' ') {
            field.append(" · ");
          } else {
            field.append("   ");
          }
        }

        // Sonstwas?
        else {
          throw new RuntimeException("?? " + coor2obj.get(new Coordinate(x, y)).getClass());
        }
      }
      field.append("\r\n");
    }

    return field.toString();
  }

  /**
   * Richtung eines Rätselwortes (runter oder nach rechts).
   */
  enum Direction {
    RIGHT(1, 0, "→"), DOWN(0, 1, "↓");

    final int dx;
    final int dy;
    final String arrow;

    Direction(int dx, int dy, String arrow) {
      this.dx = dx;
      this.dy = dy;
      this.arrow = arrow;
    }
  }

  /**
   * Koordinate im Raster.
   */
  static class Coordinate {
    final int x;
    final int y;

    Coordinate(int x, int y) {
      this.x = x;
      this.y = y;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Coordinate that = (Coordinate) o;
      return x == that.x &&
        y == that.y;
    }

    @Override
    public int hashCode() {
      return Objects.hash(x, y);
    }

    public Coordinate change(Direction direction) {
      return new Coordinate(x + direction.dx, y + direction.dy);
    }

    public Coordinate change(Direction direction, int factor) {
      return new Coordinate(x + direction.dx * factor, y + direction.dy * factor);
    }
  }

  /**
   * Eine einzelne Frage im Kreuzworträtsel.
   */
  static class Quest {
    final String question;
    final String answer;
    String id;
    Direction direction;

    Quest(String question, String answer) {
      this.question = question;
      this.answer = answer;
    }

    public void setID(int id) {
      this.id = Integer.toString(id);
      this.id = "  ".substring(this.id.length()) + this.id;
    }
  }

  /**
   * Eine einzelne Frage im Kreuzworträtsel, schon plaziert und mit Richtung.
   */
  static class PlacedQuest {
    final Quest quest;
    final Coordinate coordinate;
    final Direction direction;

    PlacedQuest(Quest quest, Coordinate coordinate, Direction direction) {
      this.quest = quest;
      this.coordinate = coordinate;
      this.direction = direction;
    }
  }

  /**
   * Platzierte Frage, bewertet (Angabe von Überschneidungen mit anderen Wörtern).
   */
  static class ScoredPlace {
    final PlacedQuest placedQuest;
    final int score;

    ScoredPlace(PlacedQuest placedQuest, int score) {
      this.placedQuest = placedQuest;
      this.score = score;
    }
  }
}
