package com.packenius.crosswordpuzzlegenerator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class WordInput {
  private static JTextArea textArea = new JTextArea("Frage 1\r\nAntwort 1\r\n\r\nFrage 2\r\nAntwort 2\r\n\r\n...");
  private static JButton button = new JButton("Kreuzworträtsel erstellen");

  public static void main(String[] args) {
    JFrame frame = new JFrame("Crossword Puzzle Generator");
    frame.setSize(800, 600);
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.setLocationRelativeTo(null);
    frame.add(textArea, BorderLayout.CENTER);
    frame.add(button, BorderLayout.SOUTH);
    button.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        String text = textArea.getText();
        String[] lines = text.replace("\r\n", "\n").replace('\r', '\n').split("\n");
        try {
          CrosswordPuzzleGenerator.createAndOpenPuzzle(new ArrayList<>(Arrays.asList(lines)), 1000);
        } catch (Exception ex) {
          JOptionPane.showMessageDialog(frame, "Beim Erzeugen des Rätsels trat ein Fehler auf. Sorry.\r\n"
            + ex.getMessage());
        }
      }
    });
    frame.setVisible(true);
  }
}
