# Crossword Puzzle Generator

Mit diesem Tool können Kreuzworträtsel erstellt werden. Sie werden als
HTML-Dokument erzeugt, die Rahmen werden mittels entsprechenden Unicode-Zeichen
gezeichnet.

Einfach in der main()-Methode die Methode workInputFile() mit der
entsprechenden Textdatei aufrufen, das Ergebnis wird dann im Browser
dargestellt.

Die Textdatei kann Leerzeilen und mit '#' startende Kommentarzeilen
enthalten. Beide werden ignoriert.

Jeweils eine Frage im Kreuzworträtsel muss mit zwei Zeilen in der Textdatei
beschrieben werden. Die erste Zeile stellt die "Frage" dar, die zweite
die auszufüllende "Antwort". Kommt in der Antwort ein Leerzeichen vor, wird
dies durch einen kleinen Punkt im entsprechenden Feld angegeben.

Falls mitten im Kreuzworträtsel ein einzelnes Kästchen vorkommt, das zwar
ungenutzt ist, aber von allen vier Seiten umgeben ist, wird dies mit
"###" gefüllt.

